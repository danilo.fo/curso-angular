import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CSon2Component } from './c-son2.component';

describe('CSon2Component', () => {
  let component: CSon2Component;
  let fixture: ComponentFixture<CSon2Component>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CSon2Component]
    });
    fixture = TestBed.createComponent(CSon2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
