import {Component} from '@angular/core';
import {_auxService} from "../services/_aux.service";

@Component({
  selector: 'app-c-son2',
  templateUrl: './c-son2.component.html',
  styleUrls: ['./c-son2.component.scss']
})
export class CSon2Component {
  constructor(public _aux: _auxService) {
  }
}
