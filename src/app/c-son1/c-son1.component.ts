import {Component} from '@angular/core';
import {_auxService} from "../services/_aux.service";

@Component({
  selector: 'app-c-son1',
  templateUrl: './c-son1.component.html',
  styleUrls: ['./c-son1.component.scss']
})
export class CSon1Component {
  constructor(public _aux: _auxService) {
  }
}
