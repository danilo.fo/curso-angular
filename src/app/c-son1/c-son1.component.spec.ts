import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CSon1Component } from './c-son1.component';

describe('CSon1Component', () => {
  let component: CSon1Component;
  let fixture: ComponentFixture<CSon1Component>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CSon1Component]
    });
    fixture = TestBed.createComponent(CSon1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
