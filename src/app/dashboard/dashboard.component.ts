import {Component, ViewChild} from '@angular/core';
import {FormControl, FormGroup, FormGroupDirective, NgForm, Validators} from "@angular/forms";
import {_auxService} from "../services/_aux.service";
import {MatDialog} from "@angular/material/dialog";
import {ConfirmDialogComponent} from "../confirm-dialog/confirm-dialog.component";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-root',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})


export class DashboardComponent {

  @ViewChild('myForm') myForm!: NgForm;

  constructor(public _aux: _auxService, public dialog: MatDialog, private http: HttpClient) {

    this.obterLista()

    this.rastreioStatus()
  }

  title = 'meu-projeto';
  size: string = '16px';
  class: boolean = true
  ngIfText: boolean = true;

  list: any[] = [
    {nome: 'Danilo Fernando', idade: '30'},
    {nome: 'Murillo Lima', idade: '35'},
    {nome: 'Gabreil K.', idade: '23'},
    {nome: 'Marcos Túlio', idade: '22'},
  ]

  disabledInput: boolean = true;
  switch: string = 'type1';


  nomeCompleto: string = '';
  campoNgForNome: string = '';
  campoNgForIdade: string = '';

  formGroup = new FormGroup({
    subject: new FormControl({value: '', disabled: false}, [
      Validators.required,
      Validators.minLength(3),
    ]),
    note: new FormControl('Anotação aqui')
  })
  Notes: any[] = [];
  campoPai: string = '';
  logStatus: any[] = [];


  fontSize(size: string) {
    this.size = size;
  }

  changeClass() {

    this.class = !this.class;
  }

  changeNgIf() {
    this.ngIfText = !this.ngIfText
  }


  changeDisabled() {
    this.disabledInput = !this.disabledInput
  }

  changeSwicth(type: string) {

    this.switch = type

  }

  inserirList(nome: any, idade: any) {

    this.list.push({
      nome: nome,
      idade: idade.toString()
    })

    window.localStorage.setItem('lista', JSON.stringify(this.list))

    this.campoNgForNome = ''
    this.campoNgForIdade = ''


  }

  private obterLista() {
    let list = window.localStorage.getItem('lista')

    if (list) {
      this.list = JSON.parse(list);
    }
  }

  removeList(index: any) {

    let remove = this.dialog.open(ConfirmDialogComponent, {
      data: {
        msg: 'Deseja remover esse indivíduo?',
        textCancel: 'Cancelar',
        textOK: 'Remover',
      }
    })

    remove.afterClosed().subscribe(v => {
      if (v) {
        this.list.splice(index, 1)
        window.localStorage.setItem('lista', JSON.stringify(this.list))
      }
    })


  }


  submit(f: FormGroupDirective) {

    this.addNote()

    f.resetForm()

  }

  addNote() {

    let value = {
      subject: this.formGroup.get('subject')?.value,
      note: this.formGroup.get('note')?.value,
    }

    this.Notes.push(value)

  }


  setNote() {

    this.formGroup.get('subject')?.setValue('Assunto Padrão')
    this.formGroup.get('note')?.setValue('Anotação Padrão')

  }

  atualizarCampoPai($event: any) {
    this.campoPai = $event
  }

  changeStatus() {

    this._aux.status.next(!this._aux.status.getValue())

  }

  private rastreioStatus() {

    this._aux.status.subscribe(v => {

      this.logStatus.push('A variável foi alterada')
    })
  }

  openDialog() {

    this.dialog.open(ConfirmDialogComponent, {
      data: {
        msg: 'Deseja remover esse indivíduo?',
        textCancel: 'Cancelar',
        textOK: 'Remover',
      }
    })


  }

  upload($event: any) {
    let selectedFile = $event.target.files[0];

    let formData = new FormData();

    formData.append('file', selectedFile)

    this.http.post('http://127.0.0.1/minha-api/upload', formData).subscribe(v => {
      console.log(v)
    })

  }


  async uploadFile($event: any) {

    let file = $event.target.files[0];
    let formData = new FormData();
    formData.append('file', file);
    let url = 'http://127.0.0.1/minha-api/upload';


    await this.http.post(url, formData).subscribe(v => {
      console.log(v);
    })


  }
}
