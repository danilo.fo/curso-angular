import {Component, Input, SkipSelf} from '@angular/core';
import {ControlContainer} from "@angular/forms";

@Component({
  selector: 'app-c-filho2',
  templateUrl: './c-filho2.component.html',
  styleUrls: ['./c-filho2.component.scss'],
  viewProviders: [
    {
      provide: [ControlContainer],
      useFactory: (container: ControlContainer) => container,
      deps: [[new SkipSelf(), ControlContainer]],
    },
  ]
})
export class CFilho2Component {

  @Input() nomePaciente: string = '';

}
