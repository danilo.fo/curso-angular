import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CFilho2Component } from './c-filho2.component';

describe('CFilho2Component', () => {
  let component: CFilho2Component;
  let fixture: ComponentFixture<CFilho2Component>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CFilho2Component]
    });
    fixture = TestBed.createComponent(CFilho2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
