import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {C1Component} from "./c1/c1.component";
import {C2Component} from './c2/c2.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CFilhoComponent} from './c-filho/c-filho.component';
import {CFilho2Component} from './c-filho2/c-filho2.component';
import {CSon1Component} from './c-son1/c-son1.component';
import {CSon2Component} from './c-son2/c-son2.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatInputModule} from "@angular/material/input";
import {MatIconModule} from "@angular/material/icon";
import {MatDialogModule} from "@angular/material/dialog";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {LoginComponent} from './login/login.component';
import {MatButtonModule} from "@angular/material/button";
import {HttpClientModule} from "@angular/common/http";
import {BlogListComponent} from './blog/blog-list/blog-list.component';
import {BlogHeadComponent} from './blog/blog-head/blog-head.component';
import {MatCardModule} from "@angular/material/card";
import {BlogSingleComponent} from './blog/blog-single/blog-single.component';
import {BlogListAdminComponent} from './admin/blog-list-admin/blog-list-admin.component';
import {MatTableModule} from "@angular/material/table";
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {MatPaginatorModule} from "@angular/material/paginator";
import {FormPostComponent} from './admin/form-post/form-post.component';
import {CKEditorModule} from "@ckeditor/ckeditor5-angular";
import { BreadcrumbComponent } from './blog/breadcrumb/breadcrumb.component';
import { HeaderComponent } from './admin/header/header.component';
import {MatToolbarModule} from "@angular/material/toolbar";

@NgModule({
  declarations: [
    AppComponent,
    C1Component,
    C2Component,
    CFilhoComponent,
    CFilho2Component,
    CSon1Component,
    CSon2Component,
    DashboardComponent,
    LoginComponent,
    BlogListComponent,
    BlogHeadComponent,
    BlogSingleComponent,
    BlogListAdminComponent,
    FormPostComponent,
    BreadcrumbComponent,
    HeaderComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        MatInputModule,
        MatIconModule,
        MatDialogModule,
        MatButtonModule,
        HttpClientModule,
        MatCardModule,
        MatTableModule,
        MatButtonToggleModule,
        MatPaginatorModule,
        CKEditorModule,
        MatToolbarModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
