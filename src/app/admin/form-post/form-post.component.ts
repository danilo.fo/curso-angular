import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import {HttpClient} from "@angular/common/http";
import {MatDialog} from "@angular/material/dialog";
import {ConfirmDialogComponent} from "../../confirm-dialog/confirm-dialog.component";
import {ActivatedRoute, Router} from "@angular/router";
import {BlogServicesService} from "../../services/blog-services.service";
import {Blog} from "../../services/interfaces/blog";
import {Observable} from "rxjs";

@Component({
  selector: 'app-form-post',
  templateUrl: './form-post.component.html',
  styleUrls: ['./form-post.component.scss']
})
export class FormPostComponent {
  urlImages = 'http://127.0.0.1/minha-api/public/uploads/';
  @ViewChild('fileInput') fileInput!: ElementRef;
  post: any;
  imageSelected: string | boolean = false;
  public Editor = ClassicEditor;
  FG = new FormGroup({
    id: new FormControl({value: null, disabled: false}, []),
    title: new FormControl({value: '', disabled: false}, [Validators.required, Validators.maxLength(255)]),
    uri: new FormControl({value: '', disabled: false}, [Validators.required, Validators.maxLength(255)]),
    description: new FormControl({value: '', disabled: false}, [Validators.maxLength(255)]),
    text: new FormControl({value: '', disabled: false}, []),
  })


  constructor(
    private http: HttpClient,
    private dialog: MatDialog,
    private _route: ActivatedRoute,
    private _router: Router,
    private _blog: BlogServicesService
  ) {


    let id = this._route.snapshot.params['id'];
    if (id) {
      this.getPost(id).then((response) => {
        if (response) {
          this.setForm();
        } else {
          this.dialogError()
        }
      });
    }


  }

  setForm(): void {
    console.log(this.post);
    this.setId(this.post.id)
    this.setUri(this.post.uri)
    this.setTitle(this.post.title)
    this.setDescription(this.post.description)
    this.setText(this.post.text)
    this.setImage(this.post.image)
  }

  setId(value: any) {
    this.FG.controls.id.setValue(value);
  }

  setUri(value: string) {
    this.FG.controls.uri.setValue(value);
  }

  setTitle(value: string) {
    this.FG.controls.title.setValue(value);
  }

  setDescription(value: string) {
    this.FG.controls.description.setValue(value);
  }

  setText(value: string) {
    this.FG.controls.text.setValue(value);
  }

  setImage(value: string) {
    if (value && value.length > 0) {
      this.imageSelected = value;
    }
  }


  async getPost(id: number | string) {

    return new Promise((resolve) => {

      this._blog.getById(id).subscribe(v => {

        if (v && !v.error && v.data) {
          this.post = v.data
          resolve(true)
        } else {
          resolve(false)
        }
      }, err => {
        resolve(false)

      })

    })

  }

  save() {

    let data = {
      id: this.FG.controls.id.value,
      title: this.FG.controls.title.value,
      uri: this.FG.controls.uri.value,
      description: this.FG.controls.description.value,
      text: this.FG.controls.text.value,
      image: this.imageSelected ?? '',
    }


    let url = 'http://127.0.0.1/minha-api/blog/save';

    this.http.post(url, data).subscribe((v: any) => {
      if (!v.error) {

        this.dialog.open(ConfirmDialogComponent, {
          data: {
            msg: 'Conteúdo salvo com sucesso!',
            textCancel: 'Cancelar',
            textOK: 'Remover',
          }
        })

      }

    })

  }

  openFile() {
    this.fileInput.nativeElement.click();

  }

  async upload($event: any) {
    let file = $event.target.files[0];
    let formData = new FormData();
    formData.append('file', file);
    let url = 'http://127.0.0.1/minha-api/upload';


    this.http.post(url, formData).subscribe((v: any) => {
      if (v) {
        this.imageSelected = v.file;
      }

    })
  }


  dialogError() {
    let dialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        msg: 'Erro ao consultar post',
        textCancel: 'voltar',
        textOK: 'OK',
      }
    })

    dialog.afterClosed().subscribe(v => {
      this._router.navigate(['admin/posts'])
    })
  }


}
