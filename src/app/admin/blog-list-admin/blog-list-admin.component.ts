import {Component} from '@angular/core';
import {BlogServicesService} from "../../services/blog-services.service";
import {ConfirmDialogComponent} from "../../confirm-dialog/confirm-dialog.component";
import {BehaviorSubject, debounceTime, distinctUntilChanged} from "rxjs";
import {Blog} from "../../services/interfaces/blog";
import {MatDialog} from "@angular/material/dialog";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-blog-list-admin',
  templateUrl: './blog-list-admin.component.html',
  styleUrls: ['./blog-list-admin.component.scss']

})
export class BlogListAdminComponent {

  page: number | string = 1;
  last_page = null;
  qtd = 10;
  totalItens: number = 0;

  buscar: string = '';

  blog = new BehaviorSubject<Blog[]>([]);

  dataSource!: Blog[];
  displayedColumns = ['id', 'title', 'date', 'image', 'option']


  paginator = new BehaviorSubject<any>(null)

  urlImages = 'http://127.0.0.1/minha-api/public/uploads/';
  FG = new FormGroup({
    search: new FormControl({value: '', disabled: false}, []),
  })


  constructor(private _blog: BlogServicesService, private dialog: MatDialog) {

    this.getAll()
    this.runSearch();

  }


  runSearch() {

    this.FG.controls.search.valueChanges.pipe(debounceTime(500), distinctUntilChanged()).subscribe(v => {
      this.buscar = v ?? '';
      this.page = 1;
      this.getAll();
    })

  }

  async getAll() {

    await this._blog.getAll(this.qtd, this.page, this.buscar).subscribe(v => {

      if (v.data) {
        this.blog.next(v.data)
        this.dataSource = v.data;
        this.page = v.current_page;
        this.last_page = v.last_page;
        this.totalItens = v.total;
        this.paginator.next(v.links)

      } else {
        this.dialogError();

      }

    }, err => {

      this.dialogError();

    })
  }


  dialogError(txt = null) {
    let dialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        msg: txt || 'Erro ao consultar post',
        textCancel: 'voltar',
        textOK: 'OK',
      }
    })
  }

  getLabelPaginator(index: number, label: string) {
    return index == 0 ? 'Voltar' : (index == (this.paginator.getValue().length - 1) ? 'Avançar' : label)
  }


  changePaginator(event: any) {
    this.qtd = event.pageSize;
    this.page = event.pageIndex + 1;
    this.getAll();
  }

  remove(id: any) {


    let remove = this.dialog.open(ConfirmDialogComponent, {
      data: {
        msg: 'Deseja remover o post?',
        textCancel: 'Cancelar',
        textOK: 'Remover',
      }
    })

    remove.afterClosed().subscribe(v => {
      if (v) {
        this._blog.delete(id).subscribe(v => {
          if (!v.error) {
            this.dialog.open(ConfirmDialogComponent, {
              data: {
                msg: v.msg || 'Post removido com sucesso!',
                textOK: 'voltar',
              }
            })
            this.page = 1;
            this.getAll();
          } else {
            this.dialog.open(ConfirmDialogComponent, {
              data: {
                msg: v.msg || 'Erro ao remover post!',
                textOK: 'voltar',
              }
            })
          }
        })
      }


    })


  }


}
