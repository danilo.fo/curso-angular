import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogHeadComponent } from './blog-head.component';

describe('BlogHeadComponent', () => {
  let component: BlogHeadComponent;
  let fixture: ComponentFixture<BlogHeadComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BlogHeadComponent]
    });
    fixture = TestBed.createComponent(BlogHeadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
