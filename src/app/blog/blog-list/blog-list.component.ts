import {Component} from '@angular/core';
import {ConfirmDialogComponent} from "../../confirm-dialog/confirm-dialog.component";
import {BlogServicesService} from "../../services/blog-services.service";
import {BehaviorSubject} from "rxjs";
import {Blog} from "../../services/interfaces/blog";
import {Dialog} from "@angular/cdk/dialog";
import {Router} from "@angular/router";
import {breadCrumb} from "../../services/interfaces/breadCrumb";

@Component({
  selector: 'app-blog-list',
  templateUrl: './blog-list.component.html',
  styleUrls: ['./blog-list.component.scss']
})
export class BlogListComponent {

  blog = new BehaviorSubject<Blog[]>([])
  paginator = new BehaviorSubject<any>(null)
  page = 1;
  qtd = 10;

  breadCrumb: breadCrumb[] = [
    {text: 'Blog', uri: '/blog'}
  ];


  constructor(private _blog: BlogServicesService, private dialog: Dialog, private route: Router) {

    this.getAll(this.page, this.qtd);


  }


  async getAll(page: number | string, qtd: number | string) {

    await this._blog.getAll(qtd, page).subscribe(v => {

      if (v.data) {
        this.blog.next(v.data)
        this.paginator.next(v.links)

      } else {

        throw new Error('erro')

      }


    }, err => {

      this.dialog.open(ConfirmDialogComponent, {
        data: {
          msg: err.msg || 'Erro ao criar usuário.',
          textCancel: 'voltar',
          textOK: 'OK',
        }
      })

    })
  }

  goPost(uri: string) {
    if (uri) {
      this.route.navigate(['blog/' + uri])
    }
  }
}
