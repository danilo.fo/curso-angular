import {Component, Input} from '@angular/core';
import {breadCrumb} from "../../services/interfaces/breadCrumb";


@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent {

  @Input() items!: breadCrumb[];

}
