import {Component} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Blog} from "../../services/interfaces/blog";
import {BlogServicesService} from "../../services/blog-services.service";
import {ConfirmDialogComponent} from "../../confirm-dialog/confirm-dialog.component";
import {MatDialog} from "@angular/material/dialog";
import {breadCrumb} from "../../services/interfaces/breadCrumb";


@Component({
  selector: 'app-blog-single',
  templateUrl: './blog-single.component.html',
  styleUrls: ['./blog-single.component.scss']
})
export class BlogSingleComponent {

  post!: Blog

  blog: Blog[] = []
  breadCrumb: breadCrumb[] = [
    {text: 'Blog', uri: '/blog'}
  ];

  constructor(
    private activtedRoute: ActivatedRoute,
    private route: Router,
    private _blog: BlogServicesService,
    private dialog: MatDialog) {

    let uri = this.activtedRoute.snapshot.params['uri'];

    this.getPost(uri)
    this.getAll()

  }


  async getPost(uri: string) {


    await this._blog.get(uri).subscribe(v => {
      if (v.data) {
        this.post = v.data
        this.breadCrumb.push({text: this.post.title, uri: '/blog/' + uri})
      } else {
        this.dialogError()
      }
    }, err => {
      this.dialogError()


    })
  }


  async getAll() {

    await this._blog.getAll(3, 1).subscribe(v => {
      if (v.data) {
        this.blog = v.data
      }
    }, err => {

    })
  }


  dialogError() {
    let dialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        msg: 'Erro ao consultar post',
        textCancel: 'voltar',
        textOK: 'OK',
      }
    })

    dialog.afterClosed().subscribe(v => {
      this.route.navigate(['blog'])
    })
  }

}
