import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from "./dashboard/dashboard.component";
import {LoginComponent} from "./login/login.component";
import {isLoggedGuard} from "./services/is-logged.guard";
import {BlogListComponent} from "./blog/blog-list/blog-list.component";
import {BlogSingleComponent} from "./blog/blog-single/blog-single.component";
import {BlogListAdminComponent} from "./admin/blog-list-admin/blog-list-admin.component";
import {FormPostComponent} from "./admin/form-post/form-post.component";
import {isNoLoggedGuard} from "./services/is-no-logged.guard";


const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: '/blog'},

  {path: 'login', component: LoginComponent,canActivate: [isNoLoggedGuard]},

  /*Admin*/
  {path: 'admin/posts', component: BlogListAdminComponent, canActivate: [isLoggedGuard]},
  {path: 'admin/posts/form', component: FormPostComponent, canActivate: [isLoggedGuard]},
  {path: 'admin/posts/form/:id', component: FormPostComponent, canActivate: [isLoggedGuard]},

  /*Blog*/
  {path: 'blog', component: BlogListComponent},
  {path: 'blog/:uri', component: BlogSingleComponent},

  /*Aulas*/
  {path: 'dashboard', component: DashboardComponent, canActivate: [isLoggedGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {

}
