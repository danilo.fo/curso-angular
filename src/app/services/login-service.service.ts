import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {catchError, Observable, throwError} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class LoginServiceService {

  constructor(public http: HttpClient) {


  }


  cadastro(data: any): Observable<any> {

    let url = 'http://127.0.0.1/minha-api/create';

    let options = {
      headers: new HttpHeaders({
        'Content-type': 'application/json',
      })
    }

    return this.http.post(url, data, options).pipe(catchError((error: HttpErrorResponse) => {
      return throwError(() => {
        return error.error
      });
    }))


  }
  login(data: any): Observable<any> {

    let url = 'http://127.0.0.1/minha-api/login';

    let options = {
      headers: new HttpHeaders({
        'Content-type': 'application/json',
      })
    }

    return this.http.post(url, data, options).pipe(catchError((error: HttpErrorResponse) => {
      return throwError(() => {
        return error.error
      });
    }))


  }


}
