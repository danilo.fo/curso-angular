export interface Blog {
  id: number;
  title: string;
  uri: string;
  description: string;
  text: string;
  image: string;
  updated_at: string;
  created_at: string;

}
