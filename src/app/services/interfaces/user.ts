export interface User {
  id: number;
  name: string;
  email: string;
  password: string;
  updated_at: string;
  created_at: string;

}
