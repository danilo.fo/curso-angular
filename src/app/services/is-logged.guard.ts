import {CanActivateFn, Router} from '@angular/router';

export const isLoggedGuard: CanActivateFn = (route, state) => {

  const router = new Router();

  let login = window.localStorage.getItem('login');

  if (!login || login !== 'true') {

    router.navigate(['/login'])

    return false

  } else {

    return true
  }


};
