import {Injectable} from '@angular/core';
import {catchError, Observable, throwError} from "rxjs";
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class BlogServicesService {

  constructor(private http: HttpClient) {
  }


  getAll(qtd: any, page: any, buscar: string | number = ''): Observable<any> {

    let url = 'http://127.0.0.1/minha-api/blog';

    let options = {
      headers: new HttpHeaders({
        'Content-type': 'application/json',
      })
    }
    let data = {
      qtd: qtd,
      page: page,
      busca: buscar ?? ''
    }

    return this.http.post(url, data, options).pipe(catchError((error: HttpErrorResponse) => {
      return throwError(() => {
        return error.error
      });
    }))


  }

  get(uri: string): Observable<any> {

    let url = 'http://127.0.0.1/minha-api/blog/' + uri;


    return this.http.get(url).pipe(catchError((error: HttpErrorResponse) => {
      return throwError(() => {
        return error.error
      });
    }))


  }

  getById(id: number | string): Observable<any> {

    let url = 'http://127.0.0.1/minha-api/post/get';

    return this.http.post(url, {id: id}).pipe(catchError((error: HttpErrorResponse) => {
      return throwError(() => {
        return error.error
      });
    }))


  }

  delete(id: number | string): Observable<any> {

    let url = 'http://127.0.0.1/minha-api/post/remove';

    return this.http.post(url, {id: id}).pipe(catchError((error: HttpErrorResponse) => {
      return throwError(() => {
        return error.error
      });
    }))


  }


}
