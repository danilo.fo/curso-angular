import {CanActivateFn, Router} from '@angular/router';

export const isNoLoggedGuard: CanActivateFn = (route, state) => {
  const router = new Router();

  let login = window.localStorage.getItem('login');

  if (!login || login !== 'true') {

    return true

  } else {

    router.navigate(['/admin/posts'])

    return false


  }

};
