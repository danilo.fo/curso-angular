import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-c-filho',
  templateUrl: './c-filho.component.html',
  styleUrls: ['./c-filho.component.scss']
})
export class CFilhoComponent {

  @Input() nomePaciente: string = '';

  @Output() nomePacienteNovo = new EventEmitter();

  nomeMudou() {

    this.nomePacienteNovo.emit(this.nomePaciente);

  }


}
