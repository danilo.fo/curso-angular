import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {MatDialog} from "@angular/material/dialog";
import {ConfirmDialogComponent} from "../confirm-dialog/confirm-dialog.component";
import {catchError, Observable, throwError} from "rxjs";
import {LoginServiceService} from "../services/login-service.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  constructor(
    private http: HttpClient,
    private dialog: MatDialog,
    private _loginService: LoginServiceService,
    private router: Router
  ) {
  }

  pageCadastro: boolean = false

  loginFG = new FormGroup({
    email: new FormControl({value: '', disabled: false}, [Validators.required,]),
    password: new FormControl({value: '', disabled: false}, [Validators.required,]),
  })
  cadastroFG = new FormGroup({
    name: new FormControl({value: '', disabled: false}, [Validators.required,]),
    email: new FormControl({value: '', disabled: false}, [Validators.required,]),
    password: new FormControl({value: '', disabled: false}, [Validators.required,]),
  })

  changePage() {
    this.pageCadastro = !this.pageCadastro;
  }


  async login() {

    let data = {
      email: this.loginFG.controls.email.value,
      password: this.loginFG.controls.password.value,
    }


    await this._loginService.login(data).subscribe(v => {
      if (!v.error) {

        window.localStorage.setItem('login', 'true');
        this.router.navigate(['/admin/posts'])

      } else {
        this.dialog.open(ConfirmDialogComponent, {
          data: {
            msg: v.msg || 'Erro ao efetuar login.',
            textCancel: 'voltar',
            textOK: 'OK',
          }
        })
      }

    })


  }


  async cadastro() {

    let data = {
      name: this.cadastroFG.controls.name.value,
      email: this.cadastroFG.controls.email.value,
      password: this.cadastroFG.controls.password.value,
    }


    await this._loginService.cadastro(data).subscribe(v => {

      if (!v.error) {


        let dialog = this.dialog.open(ConfirmDialogComponent, {
          data: {msg: 'Usuário cadastrado com sucesso!', textCancel: 'voltar', textOK: 'OK',}
        })


        dialog.afterClosed().subscribe(v => {
          this.cadastroFG.reset();
          this.changePage()
        })


      } else {

        throw new Error('erro')

      }


    }, err => {
      this.dialog.open(ConfirmDialogComponent, {
        data: {
          msg: err.msg || 'Erro ao criar usuário.',
          textCancel: 'voltar',
          textOK: 'OK',
        }
      })
    })


  }


}
